export default (editor, opts = {}) => {
  const bm = editor.BlockManager;

  bm.add('grapejs-signaturebox-block', {
    label: 'SignatureBox',
    content: { type: 'grapejs-signaturebox-component' },
    // media: '<svg>...</svg>',
  });
}
