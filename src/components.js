import SignaturePad from 'signature_pad';

export default (editor, opts = {}) => {
  const c = opts;
  const domc = editor.DomComponents;
  const defaultType = domc.getType('default');
  const textType = domc.getType('text');
  const defaultModel = defaultType.model;
  const defaultView = defaultType.view;

  const nameTrait = {
    name: 'name',
    label: c.labelTraitName,
  };

  const placeholderTrait = {
    name: 'placeholder',
    label: c.labelTraitPlaceholder,
  };

  const getTraitType = value => {
    if (typeof value == 'number') return 'number';
    if (typeof value == 'boolean') return 'checkbox';
    return 'signature';
  };

  domc.addType('grapejs-signaturebox-component', {
    model: defaultModel.extend({
      defaults: {
        ...defaultModel.prototype.defaults,
        'custom-name': "Signature Pad",
        tagName: 'div',
        //content: '<div class="signature-pad">XXX</div>',
        //draggable: 'form, form *',
        droppable: false,
        attributes:{
          //class:"signature-pad"
        },
        traits: [
        ],
      },
    }, {
      isComponent(el) {
        console.log("This is my component")
        if(el.tagName == 'SIGNATURE') {
          return {type: 'canvas'};
        }
      },
    }),
    view: {
      // Be default, the tag of the element is the same of the model
      tagName: 'div',
      init({ el }) {
        
        console.log('Local hook: view.init');
      },
      onRender({ el }) {
        console.log('Local hook: view.onRender');
        //var signaturePad = new SignaturePad(div);
        const div = document.createElement('div');
        div.innerText = 'Local hook: view.onRender';
        div.classList.add("signature-pad")
        //var canvas = document.querySelector('canvas');
        
        el.appendChild(div);
      },
    }
    
  });


};
